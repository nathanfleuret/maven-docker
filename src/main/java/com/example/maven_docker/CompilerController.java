package com.example.maven_docker;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.UUID;

@RestController
@RequestMapping("/compiler")
public class CompilerController {

    @PostMapping("/python")
    public ResponseEntity<String> compilePython(@RequestBody RequestCompilation request) {
        String imageName = "registry.gitlab.com/nathanfleuret/compilers/python:latest";
        return compile(request, imageName);
    }

    @PostMapping("/java")
    public ResponseEntity<String> compileJava(@RequestBody RequestCompilation request) {
        String imageName = "registry.gitlab.com/nathanfleuret/compilers/java:latest";
        return compile(request, imageName);
    }

    @PostMapping("/dotnet")
    public ResponseEntity<String> compileDotnet(@RequestBody RequestCompilation request) {
        String imageName = "registry.gitlab.com/nathanfleuret/compilers/dotnet:latest";
        return compile(request, imageName);
    }

    private ResponseEntity<String> compile(RequestCompilation request, String imageName){
        String result = runInDocker(imageName, request.getCode());
        return ResponseEntity.ok()
                .body(result);
    }

    private String runInDocker(String imageName, String code){
        String[] dockerCommand = new String[]{"docker", "run", "--rm", imageName, code};
        ProcessBuilder processbuilder = new ProcessBuilder(dockerCommand);
        Process process = null;

        try {
            process = processbuilder.start();
            process.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        String output = null;
        String containerOutput = null;
        try {
            output = readOutput(errorReader);
            if (!output.equals(""))
                containerOutput = output;
            output = readOutput(reader);
            if (!output.equals(""))
                containerOutput = output;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return containerOutput;
    }

    private static String readOutput(BufferedReader reader) throws IOException {
        String line;
        StringBuilder builder = new StringBuilder();

        while ((line = reader.readLine()) != null) {
            builder.append(line);
            builder.append(System.getProperty("line.separator"));
        }

        return builder.toString();
    }
}

class RequestCompilation {
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String code;
}
