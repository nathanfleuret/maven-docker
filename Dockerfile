FROM openjdk:11.0.6-jre-slim
WORKDIR /compiler

USER root

COPY  target/*.jar compiler.jar

RUN apt update && apt install -y docker.io

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=$profiles", "compiler.jar"]


# Compile maven code : "./mvnw package"
# Build image by typing the following command : "docker image build . -t onlinecompiler"
# Run the container by typing the following command : "docker container run -p 8080:8080 -v /var/run/docker.sock:/var/run/docker.sock -t onlinecompiler"
# on windows : "docker container run -p 8080:8080 -v //var/run/docker.sock:/var/run/docker.sock -t onlinecompiler"
